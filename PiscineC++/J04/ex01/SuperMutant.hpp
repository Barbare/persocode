/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   SuperMutant.hpp                                    :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: mbarbari <mbarbari@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2015/06/19 09:07:17 by mbarbari          #+#    #+#             */
/*   Updated: 2015/06/19 09:14:47 by mbarbari         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#ifndef SUPERMUTANT_H
#define SUPERMUTANT_H

#include <string>
#include <iostream>

#include "AEnemy.hpp"

class SuperMutant : public AEnemy {
	protected:
	
	public:
		SuperMutant();
		~SuperMutant();
	
		void takeDamage(int damage);
	
};

#endif
