/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   AWeapon.cpp                                        :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: mbarbari <mbarbari@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2015/06/19 08:59:30 by mbarbari          #+#    #+#             */
/*   Updated: 2015/06/19 08:59:35 by mbarbari         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "AWeapon.hpp"

AWeapon::AWeapon(std::string const & name, int apcost, int damage)
: name(name), apcost(apcost), damage(damage) {

}

AWeapon::~AWeapon() {

}

std::string const & AWeapon::getName() const {
	return name;
}

int AWeapon::getAPCost() const {
	return apcost;
}

int AWeapon::getDamage() const {
	return damage;
}
