/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   AEnemy.hpp                                         :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: mbarbari <mbarbari@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2015/06/19 09:07:17 by mbarbari          #+#    #+#             */
/*   Updated: 2015/06/19 09:14:09 by mbarbari         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#ifndef AENEMY_H
#define AENEMY_H

#include <string>
#include <iostream>

class AEnemy {
	protected:
		int hp;
		std::string type;
	
	public:
		AEnemy(int hp, std::string const & type);
		virtual ~AEnemy();
	
		std::string const & getType() const;
		int getHP() const;
	
		virtual void takeDamage(int damage);
};

#endif
