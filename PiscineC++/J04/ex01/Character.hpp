/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   Character.hpp                                      :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: mbarbari <mbarbari@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2015/06/19 09:07:17 by mbarbari          #+#    #+#             */
/*   Updated: 2015/06/19 09:15:33 by mbarbari         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#ifndef CHARACTER_H
#define CHARACTER_H

#include <string>
#include <iostream>

#include "AEnemy.hpp"
#include "AWeapon.hpp"

class Character {
	protected:
		std::string name;
		int ap;
		AWeapon *weapon;
	
	public:
		Character(std::string const & name);
		~Character();
	
		void recoverAP();
		void equip(AWeapon *weapon);
		void attack(AEnemy *enemy);
		bool action(int cost);
		std::string const & getName() const;
		int getAp() const;
		AWeapon *getWeapon() const;
};

std::ostream & operator<<(std::ostream & os, Character const & perso);

#endif
