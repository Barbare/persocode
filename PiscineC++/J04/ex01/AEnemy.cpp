/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   AEnemy.cpp                                         :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: mbarbari <mbarbari@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2015/06/19 08:59:30 by mbarbari          #+#    #+#             */
/*   Updated: 2015/06/19 08:59:35 by mbarbari         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "AEnemy.hpp"

AEnemy::AEnemy(int hp, std::string const & type)
: hp(hp), type(type) {

}

AEnemy::~AEnemy() {

}

std::string const & AEnemy::getType() const {
	return type;
}

int AEnemy::getHP() const {
	return hp;
}

void AEnemy::takeDamage(int damage) {
	if (damage > 0) {
		hp -= damage;
	}
}
