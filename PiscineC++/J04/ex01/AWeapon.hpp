/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   AWeapon.hpp                                        :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: mbarbari <mbarbari@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2015/06/19 09:07:17 by mbarbari          #+#    #+#             */
/*   Updated: 2015/06/19 09:15:02 by mbarbari         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#ifndef AWEAPON_H
#define AWEAPON_H

#include <string>
#include <iostream>

class AWeapon {
	protected:
		std::string name;
		int apcost;
		int damage;
	
	public:
		AWeapon(std::string const & name, int apcost, int damage);
		virtual ~AWeapon();
	
		std::string const & getName() const;
		int getAPCost() const;
		int getDamage() const;
	
		virtual void attack() const = 0;

};

#endif
