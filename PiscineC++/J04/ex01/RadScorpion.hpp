/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   RadScorpion.hpp                                    :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: mbarbari <mbarbari@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2015/06/19 09:07:17 by mbarbari          #+#    #+#             */
/*   Updated: 2015/06/19 09:16:08 by mbarbari         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#ifndef RADSCORPION_H
#define RADSCORPION_H

#include <string>
#include <iostream>

#include "AEnemy.hpp"

class RadScorpion : public AEnemy {
	protected:
	
	public:
		RadScorpion();
		~RadScorpion();
	
};

#endif
