/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   PlasmaRifle.hpp                                    :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: mbarbari <mbarbari@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2015/06/19 09:07:17 by mbarbari          #+#    #+#             */
/*   Updated: 2015/06/19 09:15:46 by mbarbari         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#ifndef PLASMARIFLE_H
#define PLASMARIFLE_H

#include <string>
#include <iostream>

#include "AWeapon.hpp"

class PlasmaRifle : public AWeapon {
	protected:
	
	public:
		PlasmaRifle();
		virtual ~PlasmaRifle();
	
		virtual void attack() const;
};

#endif
