/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ISquad.cpp                                         :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: mbarbari <mbarbari@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2015/06/19 09:48:49 by mbarbari          #+#    #+#             */
/*   Updated: 2015/06/19 11:02:27 by mbarbari         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include <ISquad.hpp>

ISquad::ISquad() : _count(0){
	
}

ISquad::ISquad(ISquad const &rhs) {
	
}

ISquad & ISquad::operator=(ISquad const &rhs) {
	this->_count = rhs._count;
	this->_nukeTroopers = new ISpaceMarine[this->_count];
	return (*this);
}

ISquad::~ISquad() {
	if (this->_count > 0)
		delete [] _nukeTroopers;
}


int				ISquad::getCount() {
	return (this->_count);
}

ISpaceMarine	*ISquad::getUnit(int N) {
	if (this->_count > 0)
	{
		if (N > this->_cout)
			return (this->_nukeTrooper[0]);
		else
			return (this->_nukeTrooper[this->_cout - N]);
		this->_cout -= N;
	}
	else
		std::cout
			<< "Lieutenant, Vous n'avez pas de troupe a fournir" << std::endl;
}

int				ISquad::push(ISpaceMarine* newUnits) {

}

ISquad::NodeSpaceMarine::NodeSpaceMarine() : _next(NULL) {
	this->_nukeTroopers = new ISpaceMarine();
}

virtual	ISquad::NodeSpaceMarine::~NodeSpaceMarine() {
	ISquad::NodeSpaceMarine *tmp;

	tmp = this->_next;
	while (this->_next != NULL)
	{
		tmp = this->_next->_next;
		delete this->_next;
		this->_next = tmp;
	}
}

int		ISquad::NodeSpaceMarine::AddTroopers(ISquad::NodeSpaceMarine *node) {
	ISquad::NodeSpaceMarine *tmp;
	int		cmp;

	cmp = 1;
	tmp = this;
	while (tmp->_next != NULL)
	{
		tmp = tmp->_next;
		++cmp;
	}
	tmp->_next = node;
	return (++cmp);
}

int				ISquad::_count = 0;
