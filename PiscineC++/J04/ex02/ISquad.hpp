/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ISquad.hpp                                         :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: mbarbari <mbarbari@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2015/06/19 09:40:32 by mbarbari          #+#    #+#             */
/*   Updated: 2015/06/19 11:02:37 by mbarbari         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include <ISpaceMarine.hpp>

class ISquad {

	private :
		static int		_count;
		ISquad::NodeSpaceMarine* _Troopers;

	public :
		ISquad();
		ISquad(ISquad const &rhs);
		ISquad & operator=(ISquad const &rhs);
		virtual ~ISquad();
		virtual int getCount() const;
		virtual int ISpaceMarine* getUnit(int) const;
		virtual int push(ISpaceMarine*);
};

class ISquad::NodeSpaceMarine
{
	protected :
		ISpaceMarine*	_nukeTroopers;
		ISquad::NodeSpaceMarine		*_next;
		bool						*isManager;

	public :
		NodeSpaceMarine();
		virtual	~NodeSpaceMarine();
		void	AddTroopers(ISquad::NodeSpaceMarine*);
};

#endif
