/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   Victim.hpp                                         :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: mbarbari <mbarbari@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2015/06/18 10:43:07 by mbarbari          #+#    #+#             */
/*   Updated: 2015/06/18 17:23:14 by mbarbari         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#ifndef VICTIM_HPP
# define VICTIM_HPP

#include <string>
#include <fstream>

class Victim {

	private :
		std::string _name;

	public :
		Victim(std::string name);
		Victim(Victim const &rhs);
		Victim & operator=(Victim const &rhs);
		virtual ~Victim();
		virtual std::string	introduce(void) const;
		void		getPolymorphed() const;
};

std::ostream& operator<<(std::ostream& ofs, Victim const& rhs);

#endif
