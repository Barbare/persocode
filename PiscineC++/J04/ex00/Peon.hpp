/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   Peon.hpp                                           :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: mbarbari <mbarbari@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2015/06/18 14:14:12 by mbarbari          #+#    #+#             */
/*   Updated: 2015/06/18 17:28:49 by mbarbari         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#ifndef PEON_HPP
# define PEON_HPP

#include "Victim.hpp"

class Peon : public Victim{

	private :
		std::string _name;

	public :
		Peon(std::string name);
		Peon(Peon const &rhs);
		Peon & operator=(Peon const &rhs);
		void getPolymorphed(void) const;
		virtual ~Peon();
};

#endif
