/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   Victim.cpp                                         :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: mbarbari <mbarbari@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2015/06/18 10:41:01 by mbarbari          #+#    #+#             */
/*   Updated: 2015/06/18 17:28:23 by mbarbari         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include <Victim.hpp>
#include <iostream>

Victim::Victim(std::string name) : _name(name){
	std::cout
		<< "Some random victim called " << name << " just popped" << std::endl;
}

Victim::Victim(Victim const &rhs) {
	this->_name = rhs._name;	
}

Victim& Victim::operator=(Victim const &rhs) {
	this->_name = rhs._name;
	return (*this);
}

Victim::~Victim() {
	std::cout
		<< "Victim " << this->_name << " just died for no apparent reason !"
		<< std::endl;
}

std::string	Victim::introduce(void) const{
	return (std::string("I'm " + this->_name + " and i like otters !\n")
			);
}

void		Victim::getPolymorphed() const {
	std::cout
		<< this->_name << " has been turned into a cute little sheep !"
		<< std::endl;
}

std::ostream& operator<<(std::ostream& ofs, Victim const& rhs) {
	ofs << rhs.introduce();
	return (ofs);
}
