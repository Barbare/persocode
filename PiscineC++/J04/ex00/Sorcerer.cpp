/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   Sorcerer.cpp                                       :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: mbarbari <mbarbari@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2015/06/17 19:46:42 by mbarbari          #+#    #+#             */
/*   Updated: 2015/06/18 17:15:43 by mbarbari         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */


#include <Sorcerer.hpp>
#include <iostream>

Sorcerer::Sorcerer(std::string name, std::string title) :	_name(name),
															_title(title) {
	std::cout << name << ", " << title << ", is born !" << std::endl;
}

Sorcerer::Sorcerer(Sorcerer const &rhs) {
	*this = rhs;
}

std::string	Sorcerer::introduce(void) const{
	return ("I am " + this->_name + ", " + this->_title
		+ ", and I like the ponies !\n");
}

Sorcerer & Sorcerer::operator=(Sorcerer const &rhs) {
	this->_name = rhs._name;
	return (*this);
}

Sorcerer::~Sorcerer() {
	std::cout
		<<  this->_name << ", " << this->_title
		<< ", is dead. Consequences will never be the same !" << std::endl;
}

void		Sorcerer::polymorph(Victim const& rhs) const {
	rhs.getPolymorphed();
}

std::ostream& operator<<(std::ostream& ofs, Sorcerer const& rhs) {
	ofs << rhs.introduce();
	return (ofs);
}
