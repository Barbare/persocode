/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   Sorcerer.hpp                                       :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: mbarbari <mbarbari@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2015/06/17 19:49:51 by mbarbari          #+#    #+#             */
/*   Updated: 2015/06/18 14:12:32 by mbarbari         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#ifndef SORCERER_HPP
# define SORCERER_HPP

#include <string>
#include <Victim.hpp>

class Sorcerer {

	private :
		std::string _name;
		std::string _title;

	public :
		Sorcerer(std::string name, std::string title);
		Sorcerer(Sorcerer const &rhs);
		Sorcerer& operator=(Sorcerer const &rhs);
		virtual std::string	introduce(void) const;
		virtual ~Sorcerer();
		virtual void	polymorph(Victim const&rhs) const;
};

std::ostream& operator<<(std::ostream& ofs, Sorcerer const& seLaPete);


#endif
