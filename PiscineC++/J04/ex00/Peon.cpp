/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   Peon.cpp                                           :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: mbarbari <mbarbari@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2015/06/18 14:13:19 by mbarbari          #+#    #+#             */
/*   Updated: 2015/06/18 18:04:42 by mbarbari         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include <Peon.hpp>
#include <iostream>

Peon::Peon(std::string name) : Victim(name) {
	this->_name = name;
	std::cout
		<< "Zog Zog." << std::endl;
}

Peon::Peon(Peon const &rhs) : Victim(rhs._name){
	std::cout
		<< "Zog Zog." << std::endl;
}

Peon & Peon::operator=(Peon const &rhs) {
	*this = rhs;
	return (*this);
}

Peon::~Peon() {
	std::cout
		<< "Bleuark..." << std::endl;
}

void Peon::getPolymorphed(void) const {
	std::cout << this->_name + " has beem tuned int a pink pony!" << std::endl;
}
