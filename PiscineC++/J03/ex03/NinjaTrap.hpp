/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   NinjaTrap.hpp                                       :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: mbarbari <mbarbari@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2015/06/16 10:16:20 by mbarbari          #+#    #+#             */
/*   Updated: 2015/06/19 00:50:19 by mbarbari         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#ifndef NinjaTRAP_HPP
# define NinjaTRAP_HPP

#include <iostream>
#include <ClapTrap.hpp>
#include <ScavTrap.hpp>
#include <FragTrap.hpp>

class NinjaTrap : public ClapTrap {

	private :

	public :
		NinjaTrap(std::string name);
		NinjaTrap(NinjaTrap const& rhs);
		NinjaTrap& operator=(NinjaTrap const &);
		~NinjaTrap();

		size_t		ninjaShoebox(ScavTrap const& rhs);
		size_t		ninjaShoebox(NinjaTrap const& rhs);
		size_t		ninjaShoebox(FragTrap const& rhs);
};

#endif
