/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   SuperTrap.cpp                                       :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: mbarbari <mbarbari@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2015/06/16 10:16:13 by mbarbari          #+#    #+#             */
/*   Updated: 2015/06/18 19:16:10 by mbarbari         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include <SuperTrap.hpp>
#include <NinjaTrap.hpp>
#include <FragTrap.hpp>
#include <ctime>
#include <cstdlib>

SuperTrap::SuperTrap(std::string name) :
						ClapTrap(100, 100, 100, 100, 1, name, 30, 20, 5),
						FragTrap(name), NinjaTrap(name){
	std::cout
		<< "\033[33mFR4F-TP NEW COMMAND - FragTrap\033[0m"  << std::endl;
}

SuperTrap::SuperTrap(SuperTrap const &rez) : 
						ClapTrap(100, 100, 100, 100, 1, rez._name, 30, 20, 5),
						FragTrap(rez._name), NinjaTrap(rez._name){
}

SuperTrap& SuperTrap::operator=(SuperTrap const& rhs) {
	ClapTrap::operator=(rhs);
	return (*this);
}

SuperTrap::~SuperTrap() {
	std::cout 
		<< "\033[33mFR4F-TP "<< "NEW DESTRUCTION\033[0m" << std::endl;
}

size_t		SuperTrap::SuperShoebox(std::string const& target)
{
	static int	tryit = 0;
	int			val = tryit;
	std::string nameAttack[] = {
		"You takes the biscuit?", "It smells like trouble",
		"Any pedicurist?", "Looks up, a Donkey!",
		"Ladies 'time... euh les anglais debarques c'est plus classe..." };
	std::cout << "\033[34m";
	if (this->_energyPts < 25)
	{
		std::cout << "FR4F-TP "<< "Young padawan, You don't enought rested"
			<< " for attack again!" << std::endl;
		return (0);
	}
	this->_energyPts -= 25;
	srand(time(NULL));
	while (val == tryit)
		val = rand() % 5;
	std::cout << this->getName() << " : " << nameAttack[val] << std::endl;
	std::cout << "\033[0m";
	return ((tryit = val));
}

void		SuperTrap::beRepaired(size_t amount) {
	ClapTrap::beRepaired(amount);
}
void		SuperTrap::rangedAttack(std::string const& target) {
	ClapTrap::rangedAttack(target);
}
void		SuperTrap::meleeAttack(std::string const& target) {
	ClapTrap::meleeAttack(target);
}
void		SuperTrap::takeDamage(size_t amount) {
	ClapTrap::takeDamage(amount);
}
