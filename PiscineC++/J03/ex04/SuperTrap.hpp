/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   SuperTrap.hpp                                       :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: mbarbari <mbarbari@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2015/06/16 10:16:20 by mbarbari          #+#    #+#             */
/*   Updated: 2015/06/18 18:36:06 by mbarbari         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#ifndef SUPERTRAP_HPP
# define SUPERTRAP_HPP

#include <iostream>
#include <ClapTrap.hpp>
#include <NinjaTrap.hpp>
#include <FragTrap.hpp>

class SuperTrap : public NinjaTrap, public FragTrap {

	private :

	public :
		SuperTrap(std::string name);
		SuperTrap(SuperTrap const& rhs);
		SuperTrap& operator=(SuperTrap const &);
		~SuperTrap();

		size_t		SuperShoebox(std::string const& target);
		void		beRepaired(size_t amount);
		void		rangedAttack(std::string const& target);
		void		meleeAttack(std::string const& target);
		void		takeDamage(size_t amount);
};

#endif
