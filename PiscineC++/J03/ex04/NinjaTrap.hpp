/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   NinjaTrap.hpp                                       :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: mbarbari <mbarbari@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2015/06/16 10:16:20 by mbarbari          #+#    #+#             */
/*   Updated: 2015/06/18 19:07:30 by mbarbari         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#ifndef NinjaTRAP_HPP
# define NinjaTRAP_HPP

#include <iostream>
#include <ClapTrap.hpp>

class NinjaTrap : public virtual ClapTrap {

	private :

	public :
		NinjaTrap(std::string name);
		NinjaTrap(NinjaTrap const& rhs);
		NinjaTrap& operator=(NinjaTrap const &);
		~NinjaTrap();

		//TODO: Attentin ninha Sho pas bomn
		size_t		ninjaShoebox(std::string const& target);
};

#endif
