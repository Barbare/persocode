/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   NinjaTrap.cpp                                       :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: mbarbari <mbarbari@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2015/06/16 10:16:13 by mbarbari          #+#    #+#             */
/*   Updated: 2015/06/17 16:48:57 by mbarbari         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include <NinjaTrap.hpp>
#include <ClapTrap.hpp>
#include <ctime>
#include <cstdlib>

NinjaTrap::NinjaTrap(std::string name) :
						ClapTrap(60, 60, 120, 120, 1, name, 60, 5, 0) {
	std::cout
		<< "\033[33mFR4F-TP NEW COMMAND - FragTrap\033[0m"  << std::endl;
}

NinjaTrap::NinjaTrap(NinjaTrap const &rez) : ClapTrap(rez){
}

NinjaTrap& NinjaTrap::operator=(NinjaTrap const& rhs) {
	ClapTrap::operator=(rhs);
	return (*this);
}

NinjaTrap::~NinjaTrap() {
	std::cout 
		<< "\033[33mFR4F-TP "<< "NEW DESTRUCTION\033[0m" << std::endl;
}

size_t		NinjaTrap::ninjaShoebox(std::string const& target)
{
	static int	tryit = 0;
	int			val = tryit;
	std::string nameAttack[] = {
		"You takes the biscuit?", "It smells like trouble",
		"Any pedicurist?", "Looks up, a Donkey!",
		"Ladies 'time... euh les anglais debarques c'est plus classe..." };
	std::cout << "\033[34m";
	if (this->_energyPts < 25)
	{
		std::cout << "FR4F-TP "<< "Young padawan, You don't enought rested"
			<< " for attack again!" << std::endl;
		return (0);
	}
	this->_energyPts -= 25;
	srand(time(NULL));
	while (val == tryit)
		val = rand() % 5;
	std::cout << this->getName() << " : " << nameAttack[val] << std::endl;
	std::cout << "\033[0m";
	return ((tryit = val));
}
