/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   FragTrap.hpp                                       :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: mbarbari <mbarbari@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2015/06/16 10:16:20 by mbarbari          #+#    #+#             */
/*   Updated: 2015/06/18 19:06:36 by mbarbari         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#ifndef FRAGTRAP_HPP
# define FRAGTRAP_HPP

#include <iostream>
#include <ClapTrap.hpp>

class FragTrap : public virtual ClapTrap {

	private :

	public :
		FragTrap(std::string name);
		FragTrap(FragTrap const& rhs);
		FragTrap& operator=(FragTrap const &);
		~FragTrap();

		size_t	vaulthunter_dot_exe(std::string const& target);
};

#endif
