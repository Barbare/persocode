/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   main.cpp                                           :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: mbarbari <mbarbari@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2015/06/25 17:29:32 by mbarbari          #+#    #+#             */
/*   Updated: 2015/06/25 18:28:21 by mbarbari         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include <easyfind.hpp>
#include <iostream>
#include <list>

int		main(void)
{
	std::list<int>			lst;
	lst.push_back(200);
	lst.push_back(300);
	lst.push_back(400);
	lst.push_back(500);
	lst.push_back(600);

	std::cout << "Lecture d'un find : " << std::endl << "\t" << easyfind(lst, 400) << std::endl;
	if (easyfind(lst, 900) != 0)
		std::cout << "Lecture d'un find : " << std::endl << "\t" << easyfind(lst, 900) << std::endl;
}
