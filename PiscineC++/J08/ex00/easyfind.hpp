/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   easyfind.hpp                                       :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: mbarbari <mbarbari@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2015/06/25 18:20:26 by mbarbari          #+#    #+#             */
/*   Updated: 2015/06/25 18:24:21 by mbarbari         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include <list>
#include <iostream>

template<typename T>
int		easyfind(T contener, int value)
{
	std::list<int>::const_iterator it = contener.begin();
	std::list<int>::const_iterator it2 = contener.end();
	while (it != it2)
	{
		if (*it == value)
			return (*it);
		++it;
	}
	std::cout << "Impossible de trouver la valeur : " << value << std::endl;
	return (0);
}
