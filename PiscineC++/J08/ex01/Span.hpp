/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   Span.hpp                                           :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: mbarbari <mbarbari@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2015/06/25 19:03:31 by mbarbari          #+#    #+#             */
/*   Updated: 2015/06/26 23:04:23 by mbarbari         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

# ifndef SPAN_HPP
# define SPAN_HPP

#include <vector>
#include <string>
#include <exception>

class Span {

	private :
		std::vector<int>		_nint;
		size_t						_size_int;
		Span & operator=(Span const &rhs);
		Span(void);

	public :
		Span(size_t n);
		Span(Span const &rhs);
		virtual ~Span();
		void		addNumber(int number);
		int			shortestSpan();
		int			longestSpan();
};

#endif
