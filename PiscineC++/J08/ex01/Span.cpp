/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   Span.cpp                                           :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: mbarbari <mbarbari@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2015/06/25 19:03:19 by mbarbari          #+#    #+#             */
/*   Updated: 2015/06/26 23:06:39 by mbarbari         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include <Span.hpp>
#include <algorithm>
#include <iostream>

Span::Span(size_t n) : _nint(), _size_int(n){
}

Span::Span(Span const& span) {
	(void) span;
}

Span::~Span() {
	this->_nint.clear();
}

void		Span::addNumber(int number) {
	if (this->_nint.size() < this->_size_int)
		this->_nint.push_back(number);
	else
		throw std::out_of_range("out of range");
}

int			Span::shortestSpan() {
	unsigned int		i;
	int					shortest;
	int					tmp;

	std::sort(this->_nint.begin(), this->_nint.end());
	i = 0;
	if (this->_nint.size() <= 1 ) {
		throw std::out_of_range("Not enought element");
	}
	shortest = this->_nint.back() - this->_nint.front();
	while (i + 1 < this->_nint.size())
	{
		tmp = this->_nint[i + 1] - this->_nint[i];
		if (tmp < shortest)
			shortest = tmp;
		if (shortest == 0)
			break ;
		i++;
	}
	return (shortest);
}

int			Span::longestSpan() 
{
	unsigned int		i;
	int					longest;

	std::sort(this->_nint.begin(), this->_nint.end());
	i = 0;
	if (this->_nint.size() <= 1 ) {
		throw std::out_of_range("Not enought element");
	}
	longest = this->_nint.back() - this->_nint.front();
	return (longest);
}
