/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   Iter.cpp                                           :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: mbarbari <mbarbari@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2015/06/25 13:45:03 by mbarbari          #+#    #+#             */
/*   Updated: 2015/06/25 14:06:50 by mbarbari         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include <iostream>
#include <cstdlib>
#include <ctime>

template <typename T>
void		iter(T *tab, size_t nTab, T (*func)(T))
{
	while (nTab != 0)
	{
		tab[nTab - 1] = func(tab[nTab - 1]);
		--nTab;
	}
}

int inc(int entier)
{
	return (++entier);
}

int		main(void)
{
	int		cmp = 0;
	int tab[10] = {0};

	srand(time(NULL));
	std::cout << "Ecriture aleatoire : " ;
	while (cmp < 10)
	{
		tab[cmp] = rand() % 8;
		std::cout << tab[cmp] << " - ";
		++cmp;
	}

	::iter(tab, 10, &inc); 
	cmp = 0;
	std::cout << std::endl << "Lecture apres modification : " ;
	while (cmp < 10)
	{
		std::cout << tab[cmp] << " - ";
		++cmp;
	}
	std::cout << std::endl;
	return (0);
}
