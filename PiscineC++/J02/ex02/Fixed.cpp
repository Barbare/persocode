/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   Fixed.cpp                                          :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: mbarbari <mbarbari@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2015/06/14 18:17:26 by mbarbari          #+#    #+#             */
/*   Updated: 2015/06/18 21:48:40 by mbarbari         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "Fixed.hpp"
#include <iostream>
#include <cmath>


Fixed::Fixed() : _fixe(0) {
}

Fixed::Fixed(const int fixe) : _fixe(fixe << this->_fractionalbits)  {
}

Fixed::Fixed(const float fixe) {
	this->_fixe = static_cast<int>(roundf(fixe * (1 << this->_fractionalbits)));
}

Fixed::Fixed(Fixed const &fixed) {
	*this = fixed;
}

Fixed::~Fixed() {
}

int			Fixed::getRawBits(void) const{
	return (this->_fixe);
}

void		Fixed::setRawBits(int const raw) {
	this->_fixe = raw;
}

float		Fixed::toFloat(void) const {
	return ((float)(this->_fixe) / (1 << this->_fractionalbits));
}

Fixed&		Fixed::min(Fixed& lhs, Fixed& rhs) {
	if (lhs < rhs)
		return (lhs);
	return (rhs);
}

Fixed&		Fixed::max(Fixed& lhs, Fixed& rhs) {
	if (lhs > rhs)
		return (lhs);
	return (rhs);
}

Fixed const&		Fixed::min(Fixed const& lhs, Fixed const& rhs) {
	if (lhs < rhs)
		return (lhs);
	return (rhs);
}

Fixed const&		Fixed::max(Fixed const& lhs, Fixed const& rhs) {
	if (lhs > rhs)
		return (lhs);
	return (rhs);
}

int			Fixed::toInt(void) const {
	return (this->_fixe >> _fractionalbits);
}

// Affectation =====================
Fixed & Fixed::operator=(Fixed const &rhs) {
	this->_fixe = rhs.getRawBits();
	return(*this);
}

Fixed Fixed::operator+(Fixed const &rhs) {
	Fixed newFixe = Fixed(this->getRawBits() + rhs.getRawBits());
	return newFixe;
}

Fixed Fixed::operator-(Fixed const &rhs) {
	Fixed newFixe = Fixed(this->getRawBits() - rhs.getRawBits());
	return newFixe;
}

Fixed Fixed::operator*(Fixed const &rhs) {
	Fixed    f;
	int        result;
	
	result = this->_fixe * rhs.getRawBits();
	result >>= this->_fractionalbits;
	f.setRawBits(result);
	return (f); 
}

Fixed Fixed::operator/(Fixed const &rhs) {
    Fixed    f;
    int        result;

    result = this->_fixe << _fractionalbits;
    result += rhs._fixe / 2;
    result /= rhs._fixe;

    f.setRawBits(result);
    return (f);
}

Fixed Fixed::operator++(int) {
		Fixed old(*this);
		++this->_fixe;
	return (old);
}

Fixed& Fixed::operator++() {
	++this->_fixe;
	return (*this);
}

// Comparaison =====================
bool Fixed::operator>(Fixed const &rhs) const{
	if (this->getRawBits() > rhs.getRawBits())
		return true;
	return false;
}

bool Fixed::operator<(Fixed const &rhs) const{
	if (this->getRawBits() < rhs.getRawBits())
		return true;
	return false;
}

bool Fixed::operator<=(Fixed const &rhs) const{
	if (this->getRawBits() >= rhs.getRawBits())
		return true;
	return false;
}

bool Fixed::operator>=(Fixed const &rhs) const{
	if (this->getRawBits() >= rhs.getRawBits())
		return true;
	return false;
}

bool Fixed::operator==(Fixed const &rhs) const{
	if (this->getRawBits() == rhs.getRawBits())
		return true;
	return false;
}

bool Fixed::operator!=(Fixed const &rhs) const{
	if (this->getRawBits() != rhs.getRawBits())
		return true;
	return false;
}

std::ostream& operator<<(std::ostream& ofs, Fixed const &rhs) {
	ofs << rhs.toFloat();
	return (ofs);
}

const int Fixed::_fractionalbits = 8;
