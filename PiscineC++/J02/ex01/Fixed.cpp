/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   Fixed.cpp                                          :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: mbarbari <mbarbari@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2015/06/14 18:17:26 by mbarbari          #+#    #+#             */
/*   Updated: 2015/06/18 21:09:17 by mbarbari         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "Fixed.hpp"
#include <iostream>
#include <cmath>


Fixed::Fixed() : _fixe(0) {
	std::cout << "Default constructor called" << std::endl;
}

Fixed::Fixed(const int fixe) : _fixe(fixe << this->_fractionalbits)  {
	std::cout << "Int constructor called" << std::endl;
}

Fixed::Fixed(const float fixe) : _fixe(fixe) {
	std::cout << "Float constructor called" << std::endl;
	this->_fixe = (int)(roundf(fixe * (1 << this->_fractionalbits)));
}

Fixed::Fixed(Fixed const &fixed)  {
	std::cout << "Copy constructor called" << std::endl;
	*this = fixed;
}

Fixed & Fixed::operator=(Fixed const &rhs) {
	std::cout << "Assignation operator called" << std::endl;
	this->_fixe = rhs.getRawBits();
	return (*this);
}

Fixed::~Fixed() {
	std::cout << "Destructor called" << std::endl;
}

int			Fixed::getRawBits(void) const{
	return (this->_fixe);
}

void		Fixed::setRawBits(int const raw) {
	this->_fixe = raw;
}

float		Fixed::toFloat(void) const {
	return ((float)(this->_fixe) / (1 << this->_fractionalbits));
}

int			Fixed::toInt(void) const {
	return (this->_fixe >> _fractionalbits);
}

std::ostream& operator<<(std::ostream& ofs, Fixed const &rhs) {
	ofs << rhs.toFloat();
	return (ofs);
}

const int Fixed::_fractionalbits = 8;
