# ifndef TEST_HPP
# define TEST_HPP

class Test {

	private :

	public :
		
		class execpt {
		
			private :
		
			public :
				execpt(void);
				execpt(execpt const &rhs);
				execpt & operator=(execpt const &rhs);
				virtual ~execpt();
		};
		
		#endif
		Test(void);
		Test(Test const &rhs);
		Test & operator=(Test const &rhs);
		virtual ~Test();
};

#endif
